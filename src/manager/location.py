from typing import Tuple

import sqlalchemy.exc
from fastapi import HTTPException
from sqlalchemy import select, update, Select
from starlette import status

from src.db import tables
from src.db.database import Session
from src.model.location import LocationCreate, LocationUpdate


class LocationManager:
    def __init__(self):
        self.session: Session = Session()

    async def create_location(self, create_data: LocationCreate):
        try:
            location = tables.Location(**create_data.dict())
            self.session.add(location)
            self.session.commit()
            self.session.refresh(location)
            return location
        except sqlalchemy.exc.IntegrityError as e:
            self.session.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="can't create location")

    async def update_location(self, location_id: int, update_data: LocationUpdate):
        try:
            statement = select(tables.Location).where(tables.Location.id == location_id)
            location = self.session.execute(statement).scalar()
            if not location:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                    detail=f"Location with id {location_id} not found")
            statement = (update(tables.Location).where(tables.Location.id == location_id)
                         .values(**update_data.dict(exclude_unset=True)))
            self.session.execute(statement)
            self.session.commit()
            self.session.refresh(location)
            return location
        except sqlalchemy.exc.IntegrityError as e:
            self.session.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="can't update location")

    async def get_location(
            self,
            person_id: int = None,
            country: str = None,
            city: str = None,
            street: str = None,
    ):
        statement = select(tables.Location)
        if person_id is not None:
            statement = statement.where(tables.Location.person_id == person_id)
        if country is not None:
            statement = statement.where(tables.Location.country == country)
        if city is not None:
            statement = statement.where(tables.Location.city == city)
        if street is not None:
            statement = statement.where(tables.Location.street == street)
        return self.session.execute(statement).scalars()

    async def get_location_by_id(self, location_id: int):
        statement = select(tables.Location).where(tables.Location.id == location_id)
        location = self.session.execute(statement).scalar()
        if not location:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Location with id {location_id} not found")
        return location
