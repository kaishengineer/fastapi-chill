import sqlalchemy.exc
from fastapi import HTTPException
from sqlalchemy import select, update
from starlette import status

from src.db import tables
from src.model.person import PersonCreate, Gender, EyeColor, PersonUpdate, PersonDelete
from src.db.database import Session


class PersonManager:
    def __init__(self):
        self.session: Session = Session()

    async def get_person_by_index(self, person_id):
        statement = select(tables.Person).where(tables.Person.id == person_id)
        person = self.session.execute(statement).scalar()
        if not person:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Person with id {person_id} not found")
        return person

    async def get_person(
            self,
            age_from: int = None,
            age_to: int = None,
            gender: Gender = None,
            eye_color: EyeColor = None
    ):
        statement = select(tables.Person)
        if age_from is not None:
            statement = statement.where(tables.Person.age >= age_from)
        if age_to is not None:
            statement = statement.where(tables.Person.age <= age_to)
        if gender is not None:
            statement = statement.where(tables.Person.gender == gender.value)
        if eye_color is not None:
            statement = statement.where(tables.Person.eye_color == eye_color.value)
        return self.session.execute(statement).scalars()

    async def create_person(
            self,
            create_data: PersonCreate
    ):
        try:
            person = tables.Person(**create_data.dict())
            self.session.add(person)
            self.session.commit()
            self.session.refresh(person)
            return person
        except sqlalchemy.exc.IntegrityError as e:
            self.session.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="can't create person")

    async def update_person(self, person_id: int, update_data: PersonUpdate | PersonDelete):
        try:
            statement = select(tables.Person).where(tables.Person.id == person_id)
            person = self.session.execute(statement).scalar()
            if not person:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                    detail=f"Person with id {person_id} not found")
            statement = (update(tables.Person).where(tables.Person.id == person_id)
                         .values(**update_data.dict(exclude_unset=True)))
            self.session.execute(statement)
            self.session.commit()
            self.session.refresh(person)
            return person
        except sqlalchemy.exc.IntegrityError as e:
            self.session.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="can't update person")

    async def delete_person(self, person_id):
        return await self.update_person(person_id, PersonDelete(is_deleted=True))
