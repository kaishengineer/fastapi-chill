from typing import List

from fastapi import APIRouter, Depends
from starlette import status

from src.manager.location import LocationManager
from src.model.location import LocationSelect, LocationCreate, LocationUpdate

router = APIRouter(prefix="/location", tags=["location"])


@router.get("/all", response_model=List[LocationSelect], status_code=status.HTTP_200_OK)
async def get_location(
        person_id: int = None,
        country: str = None,
        city: str = None,
        street: str = None,
        location_manager: LocationManager = Depends(),
) -> list[LocationSelect]:
    return await location_manager.get_location(person_id=person_id, country=country, city=city, street=street)


@router.get("/{location_id}", response_model=LocationSelect, status_code=status.HTTP_200_OK)
async def get_location_by_id(location_id: int, location_manager: LocationManager = Depends()) -> LocationSelect:
    return await location_manager.get_location_by_id(location_id=location_id)


@router.post("/create", response_model=LocationSelect, status_code=status.HTTP_201_CREATED)
async def create_location(create_data: LocationCreate, location_manager: LocationManager = Depends()):
    return await location_manager.create_location(create_data=create_data)


@router.put("/{location_id}", response_model=LocationSelect, status_code=status.HTTP_200_OK)
async def update_location(location_id: int, update_data: LocationUpdate, location_manager: LocationManager = Depends()):
    return await location_manager.update_location(location_id=location_id, update_data=update_data)

# to do delete function
