from typing import List

from fastapi import APIRouter, Depends
from starlette import status

from src.manager.person import PersonManager
from src.model.person import PersonCreate, PersonSelect, Gender, EyeColor, PersonUpdate

router = APIRouter(prefix="/person", tags=["person"])


@router.get("/all", response_model=List[PersonSelect], status_code=status.HTTP_200_OK)
async def get_person(
        age_from: int = None,
        age_to: int = None,
        gender: Gender = None,
        eye_color: EyeColor = None,
        person_manager: PersonManager = Depends()
) -> list[PersonSelect]:
    return await person_manager.get_person(age_to=age_to, age_from=age_from, gender=gender, eye_color=eye_color)


@router.get("/{person_id}", response_model=PersonSelect, status_code=status.HTTP_200_OK)
async def get_person_by_id(
        person_id: int,
        person_manager: PersonManager = Depends()
) -> PersonSelect:
    return await person_manager.get_person_by_index(person_id=person_id)


@router.post("/create", response_model=PersonSelect, status_code=status.HTTP_201_CREATED)
async def create_person(create_data: PersonCreate, person_manager: PersonManager = Depends()):
    return await person_manager.create_person(create_data=create_data)


@router.delete("/{person_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_person(person_id: int, person_manager: PersonManager = Depends()):
    await person_manager.delete_person(person_id=person_id)


@router.put("/{person_id}", response_model=PersonSelect, status_code=status.HTTP_200_OK)
async def update_person(person_id: int, update_data: PersonUpdate, person_manager: PersonManager = Depends()):
    return await person_manager.update_person(person_id=person_id, update_data=update_data)
