from src.api import person, location
from fastapi import APIRouter

router = APIRouter(prefix="/api")
router.include_router(person.router)
router.include_router(location.router)
