from pydantic import BaseModel


class LocationSelect(BaseModel):
    id: int
    person_id: int
    country: str
    city: str
    street: str

    class Config:
        from_attributes = True


class LocationCreate(BaseModel):
    person_id: int
    country: str
    city: str
    street: str


class LocationUpdate(BaseModel):
    country: str
    city: str
    street: str
