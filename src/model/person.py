from datetime import datetime
from enum import Enum

from pydantic import BaseModel


class Gender(Enum):
    MALE = "male"
    FEMALE = "female"


class EyeColor(Enum):
    BLUE = "blue"
    BROWN = "brown"
    GREEN = "green"


class PersonCreate(BaseModel):
    age: int
    eye_color: EyeColor
    gender: Gender
    balance: int
    registered: datetime
    email: str
    name: str

    class Config:
        use_enum_values = True


class PersonSelect(BaseModel):
    id: int
    age: int
    eye_color: EyeColor
    gender: Gender
    balance: int
    registered: datetime
    email: str
    name: str
    is_deleted: bool

    class Config:
        from_attributes = True


class PersonUpdate(BaseModel):
    age: int | None = None
    eye_color: EyeColor | None = None
    gender: Gender | None = None
    balance: int | None = None
    registered: datetime | None = None
    email: str | None = None
    name: str | None = None

    class Config:
        use_enum_values = True


class PersonDelete(BaseModel):
    is_deleted: bool = True
