from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DB_PASSWORD: str = 'password'

    DB_NAME: str = 'database name'
    DB_USER: str = 'database user'
    DB_HOST: str = 'database host'
    DB_PORT: int = 5432
    DB_DRIVER: str = 'postgresql+psycopg2'


settings = Settings(_env_file='../.env', _env_file_encoding='utf-8')
