from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from src.settings import settings


db_url = (
    f'{settings.DB_DRIVER}://{settings.DB_USER}:{settings.DB_PASSWORD}'
    f'@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}'
)

engine = create_engine(
    db_url,
    pool_recycle=3600,
    pool_pre_ping=True
)

Session = sessionmaker(engine, autocommit=False, autoflush=False)
