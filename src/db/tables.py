from datetime import datetime
from typing import Optional

from sqlalchemy import String, ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Base(DeclarativeBase):
    pass


class Person(Base):
    __tablename__ = 'person'

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    age: Mapped[int] = mapped_column(nullable=False)
    eye_color: Mapped[str] = mapped_column()
    gender: Mapped[str] = mapped_column()
    balance: Mapped[int] = mapped_column(nullable=False)
    registered: Mapped[datetime] = mapped_column(nullable=False)
    email: Mapped[str] = mapped_column()
    name: Mapped[str] = mapped_column(nullable=False)
    is_deleted: Mapped[bool] = mapped_column(nullable=False, default=False)

    location = relationship('Location', back_populates='person')


class Location(Base):
    __tablename__ = 'location'

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    country: Mapped[str] = mapped_column()
    city: Mapped[str] = mapped_column()
    street: Mapped[str] = mapped_column()

    person_id: Mapped[int] = mapped_column(ForeignKey("person.id"), nullable=False)
    person = relationship("Person", back_populates="location")
